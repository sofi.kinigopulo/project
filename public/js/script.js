function saveLocalStorage() {
  localStorage.setItem("name", $("#name").val());
   localStorage.setItem("phone", $("#phone").val());
  localStorage.setItem("email", $("#email").val());
  localStorage.setItem("message", $("#message").val());
  localStorage.setItem("personaldata", $("#personaldata").prop("checked"));
}

function loadLocalStorage() {
  if (localStorage.getItem("name") !== null) {
      $("#name").val(localStorage.getItem("name"));
  }
   if (localStorage.getItem("phone") !== null) {
      $("#phone").val(localStorage.getItem("phone"));
  }
  if (localStorage.getItem("email") !== null) {
      $("#email").val(localStorage.getItem("email"));
  }
  if (localStorage.getItem("message") !== null) {
      $("#message").val(localStorage.getItem("message"));
  }
  if (localStorage.getItem("personaldata") !== null) {
      $("#personaldata").prop("checked", localStorage.getItem("personaldata") === "true");
      if ($("#personaldata").prop("checked")) {
          $("#send").removeAttr("disabled");
      }
  }
}
function clear() {
  localStorage.clear();
  $("#name").val("");
  $("#phone").val("");
  $("#email").val("");
  $("#message").val("");
  $("#personaldata").val(false);
}

$(document).ready(function () {
  loadLocalStorage();
  $("#open").click(function () {
      $(".contact-form").css("display", "flex");
      history.pushState(true, "", "./form");
  });
  $("#close").click(function () {
      $(".contact-form").css("display", "none");
      history.pushState(false, "", ".");
  });
  $("#form").submit(function (e) {
      e.preventDefault();
      $(".contact-form").css("display", "none");
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "https://formcarry.com/s/5HT-rwOyhM",
          data: $(this).serialize(),
          success: function (response) {
              if (response.status === "success") {
                  alert("Your form has been submitted successfully");
                  clear();
              } else {
                  alert("Error: " + response.message);
              }
          }
      });
  });
  $("#personaldata").change(function () {
      if (this.checked) {
          $("#send").removeAttr("disabled");
      } else {
          $("#send").attr("disabled", "");
      }
  });
  $("#form").change(saveLocalStorage);

  window.onpopstate = function (event) {
      if (event.state) {
          $(".contact-form").css("display", "flex");
      } else {
          $(".contact-form").css("display", "none");
      }
  };
});

